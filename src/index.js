// dummy functions
const toBinary = (number) => (number >>> 0).toString(2);

const generateNumbers = n => Array.from(Array(n).keys());

// execution code
const binaries = generateNumbers(10)
  .map(number => toBinary(number));

console.log('print all binaries: \n');
binaries.forEach(bin => console.log(bin));
console.log('\n job done');
