#!/bin/bash

green=$'\e[1;32m'
yellow=$'\e[1;33m'
end=$'\e[0m'

printf "${yellow}\n"
echo "-> pre-push code check"
printf "${end} \n"

# run eslint job defined in package.json
npm run lint
printf "${green}\n-> linting succeeded ${end} \n"

npm run test
printf "${green}\n-> testing succeeded ${end} \n"